\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}

% bibliography
\usepackage[backend=biber,style=numeric,sorting=none]{biblatex}
\usepackage{csquotes}
\addbibresource{bibliography.bib}

% tables
\usepackage{changepage}
\usepackage{multirow,tabularx}

% e
\usepackage{siunitx}
\sisetup{output-exponent-marker=\ensuremath{\mathrm{e}}}

% folder structure
\usepackage{dirtree}

\title{Metodi del calcolo scientifico: progetto 1-bis}
\author{    
    Marco Magni\\
    851810
}
\date{September 2023}

\begin{document}

\maketitle
\begin{abstract}
    Implementation of a Python library to execute the following linear systems solver methods: Jacobi, Gauß-Seidel, gradient, conjugate gradient.
    
    The use of the library is limited to symmetric, positive-definite matrices.
    
    Results of the execution of the methods are shown and discussed.
\end{abstract}

\section{Introduction}

This project has been done as the first of the two assignments for Master's degree course of \textit{Metodi del calcolo scientifico (Scientific computing methods)}.

The purpose of the project is to implement a library with the desired programming language to execute the following linear systems solver methods: Jacobi, Gauß-Seidel, gradient, conjugate gradient.

The use of the library is limited to symmetric, positive-definite matrices.

In the following sections will be analyzed the architecture of the code, the data in input and the results obtained.

The project is public and available on GitLab.\cite{gitlab}

\section{Architecture}

The project has been developed using \textit{Python} programming language.

Operations with matrices have been done using Python's native operators \textit{+}, \textit{*}, and matrices multiplication operator \textit{@}.
In addition to these, the project imports and uses module and functions from \textit{SciPy} and \textit{NumPy} libraries.

The folder structure is the following (only project-strictly-related files are considered, ignoring version-control files and so on).

\vspace{10pt}

\dirtree{%
.1 LinearSystemsSolver.
.2 data.
.3 spa1.mtx.
.3 spa2.mtx.
.3 vem1.mtx.
.3 vem2.mtx.
.2 methods.
.3 conjugate$\_$gradient.py.
.3 gauss$\_$seidel.py.
.3 gradient.py.
.3 jacobi.py.
.3 method.py.
.2 utils.
.3 io.py.
.3 matrix$\_$utilities.py.
.3 settings.py.
.2 main.py.
}

\vspace{10pt}

The \textit{data} folder will be examined on its merits in Section \ref{section-inputdata}.

The \textit{methods} folder contains the \textit{method} module with a class called \textit{Method} which works as an abstract parent class for all of the others inside the folder.
In it are defined methods (intended as functions) and properties common to all methods (intended as solving method) sublasses.
Each subclass, in particular, implements its own version of the resolution iteration and other specific operations if needed.

The \textit{utils} folder contains three files.
The first one is called \textit{io}, which has the purpose to prompt the user a menu and to get his choice as an input for the program.
The second one has a self explanatory name too: \textit{matrix$\_$utilities}; it has some functions useful when dealing with matrices, regardless of linear systems resolution.
The third one, \textit{settings}, is configurable before the execution to set parameters such as: \textit{data path}, \textit{tolerances list}, \textit{methods to use}, \textit{maximum number of iterations}.

The \textit{main} is the actual program, which makes use of all of the other modules and functions, to allow the user to solve linear systems and to analyze the results.

\section{Input data}
\label{section-inputdata}

Four files have been provided as input for the analysis, and they are those that are located in the \textit{data} folder mentioned above.

These files have the \textit{.mtx} extension: this is the extension used by Matrix Market. \cite{matrix-market}
Reading .mtx files is easy if using SciPy's io.mmread function, who convert the source file in a sparse matrix in COO format. \cite{scipy-mmread}

Table \ref{tab:data} shows some stats about input matrices, including dimensions and non-zero entries, both in number and in percentage.

\begin{table}[htp]
    \centering
    \begin{tabular}{ |c|c|c c| } 
        \hline
        \textbf{Matrix} & \textbf{Dimensions} & \textbf{Non-zero entries} & \textbf{(percentage)} \\
        \hline
        spa1 & 1000x1000 & 182434 & 18,2434 \\
        \hline
        spa2 & 3000x3000 & 1633298 & 18,1478 \\
        \hline
        vem1 & 1681x1681 & 13385 & 00,4737 \\
        \hline
        vem2 & 2601x2601 & 21225 & 00,3137 \\
        \hline
    \end{tabular}
    \caption{Analysis of input matrices}
    \label{tab:data}
\end{table}

\section{Results}

The following tables are intended to show the readers the results obtained executing each linear systems solver method.

In particular, for each method, for each matrix and for each tolerance, the relative error, the number of iterations and the time taken (in seconds) have been considered.

\begin{table}[htp]
    \centering
    \begin{tabular}{ |c|c|c|c|c| } 
        \hline
        \multicolumn{5}{ |c| }{\textbf{Jacobi method}} \\
        \hline
        \textbf{Matrix} & \textbf{Tolerance} & \textbf{Relative error} & \textbf{Iterations} & \textbf{Time (s)} \\
        \hline
        \multirow{4}{*}{\textbf{spa1}} & $10^{-4}$ & 0.0017 & 116 & 0.5829 \\
        \cline{2-5}
        & $10^{-6}$ & \num{1.6771e-05} & 182 & 0.8079 \\
        \cline{2-5}
        & $10^{-8}$ & \num{1.7023e-07} & 248 & 1.1267 \\
        \cline{2-5}
        & $10^{-10}$ & \num{1.7279e-09} & 314 & 1.3195 \\
        \hline
        \multirow{4}{*}{\textbf{spa2}} & $10^{-4}$ & 0.0014 & 37 & 1.1865 \\
        \cline{2-5}
        & $10^{-6}$ & \num{1.3349e-05} & 58 & 1.8842 \\
        \cline{2-5}
        & $10^{-8}$ & \num{1.2596e-07} & 79 & 2.4422 \\
        \cline{2-5}
        & $10^{-10}$ & \num{1.1887e-09} & 100 & 3.3590 \\
        \hline
        \multirow{4}{*}{\textbf{vem1}} & $10^{-4}$ & 0.0035 & 1315 & 2.2106 \\
        \cline{2-5}
        & $10^{-6}$ & \num{3.5255e-05} & 2434 & 3.7974 \\
        \cline{2-5}
        & $10^{-8}$ & \num{3.5252e-07} & 3553 & 5.8442 \\
        \cline{2-5}
        & $10^{-10}$ & \num{3.5249e-09} & 4672 & 7.6680 \\
        \hline
        \multirow{4}{*}{\textbf{vem2}} & $10^{-4}$ & 0.0050 & 1928 & 4.4798 \\
        \cline{2-5}
        & $10^{-6}$ & \num{4.9539e-05} & 3677 & 7.3241 \\
        \cline{2-5}
        & $10^{-8}$ & \num{4.9525e-07} & 5426 & 14.0152 \\
        \cline{2-5}
        & $10^{-10}$ & \num{4.9511e-09} & 7175 & 16.4239 \\
        \hline
    \end{tabular}
    \caption{Summary of the results obtained using Jacobi method.}
    \label{tab:jacobi}
\end{table}

\begin{table}[htp]
    \centering
    \begin{tabular}{ |c|c|c|c|c| } 
        \hline
        \multicolumn{5}{ |c| }{\textbf{Gauß-Seidel method}} \\
        \hline
        \textbf{Matrix} & \textbf{Tolerance} & \textbf{Relative error} & \textbf{Iterations} & \textbf{Time (s)} \\
        \hline
        \multirow{4}{*}{\textbf{spa1}} & $10^{-4}$ & 0.0098 & 10 & 0.3588 \\
        \cline{2-5}
        & $10^{-6}$ & \num{7.0013e-05} & 18 & 0.5247 \\
        \cline{2-5}
        & $10^{-8}$ & \num{9.2088e-07} & 25 & 0.6684 \\
        \cline{2-5}
        & $10^{-10}$ & \num{1.2108e-08} & 32 & 0.7991 \\
        \hline
        \multirow{4}{*}{\textbf{spa2}} & $10^{-4}$ & 0.0007 & 6 & 0.6677 \\
        \cline{2-5}
        & $10^{-6}$ & \num{1.4006e-05} & 9 & 1.2189 \\
        \cline{2-5}
        & $10^{-8}$ & \num{7.5794e-08} & 13 & 1.4437 \\
        \cline{2-5}
        & $10^{-10}$ & \num{1.5103e-09} & 16 & 2.0730 \\
        \hline
        \multirow{4}{*}{\textbf{vem1}} & $10^{-4}$ & 0.0035 & 660 & 66.9851 \\
        \cline{2-5}
        & $10^{-6}$ & \num{3.4978e-05} & 1219 & 37.4445 \\
        \cline{2-5}
        & $10^{-8}$ & \num{3.4886e-07} & 1779 & 62.1540 \\
        \cline{2-5}
        & $10^{-10}$ & \num{3.4795e-09} & 2339 & 78.1953 \\
        \hline
        \multirow{4}{*}{\textbf{vem2}} & $10^{-4}$ & 0.0049 & 966 & 48.3145 \\
        \cline{2-5}
        & $10^{-6}$ & \num{4.9158e-05} & 1841 & 89.8609 \\
        \cline{2-5}
        & $10^{-8}$ & \num{4.9323e-07} & 2715 & 145.8943 \\
        \cline{2-5}
        & $10^{-10}$ & \num{4.9229e-09} & 3590 & 189.2354 \\
        \hline
    \end{tabular}
    \caption{Summary of the results obtained using Gauß-Seidel method.}
    \label{tab:gauss-seidel}
\end{table}

\begin{table}[htp]
    \centering
    \begin{tabular}{ |c|c|c|c|c| } 
        \hline
        \multicolumn{5}{ |c| }{\textbf{Gradient method}} \\
        \hline
        \textbf{Matrix} & \textbf{Tolerance} & \textbf{Relative error} & \textbf{Iterations} & \textbf{Time (s)} \\
        \hline
        \multirow{4}{*}{\textbf{spa1}} & $10^{-4}$ & 0.0346 & 143 & 1.2822 \\
        \cline{2-5}
        & $10^{-6}$ & 0.0010 & 3577 & 32.8186 \\
        \cline{2-5}
        & $10^{-8}$ & \num{9.8163e-06} & 8233 & 76.2773 \\
        \cline{2-5}
        & $10^{-10}$ & \num{9.8203e-08} & 12919 & 124.4653 \\
        \hline
        \multirow{4}{*}{\textbf{spa2}} & $10^{-4}$ & 0.0181 & 161 & 11.9305 \\
        \cline{2-5}
        & $10^{-6}$ & 0.0007 & 1949 & 162.7972 \\
        \cline{2-5}
        & $10^{-8}$ & \num{6.8652e-06} & 5087 & 423.3221 \\
        \cline{2-5}
        & $10^{-10}$ & \num{6.9378e-08} & 8285 & 1325.4738 \\
        \hline
        \multirow{4}{*}{\textbf{vem1}} & $10^{-4}$ & 0.0027 & 890 & 4.1880 \\
        \cline{2-5}
        & $10^{-6}$ & \num{2.7133e-06} & 1612 & 3.5914 \\
        \cline{2-5}
        & $10^{-8}$ & \num{2.6953e-08} & 2336 & 5.4938 \\
        \cline{2-5}
        & $10^{-10}$ & \num{2.7132e-10} & 3058 & 7.5018 \\
        \hline
        \multirow{4}{*}{\textbf{vem2}} & $10^{-4}$ & 0.0038 & 1308 & 3.0535 \\
        \cline{2-5}
        & $10^{-6}$ & \num{3.7914e-05} & 2438 & 6.0967 \\
        \cline{2-5}
        & $10^{-8}$ & \num{3.8099e-07} & 3566 & 8.9136 \\
        \cline{2-5}
        & $10^{-10}$ & \num{3.7988e-09} & 4696 & 11.6765 \\
        \hline
    \end{tabular}
    \caption{Summary of the results obtained using gradient method.}
    \label{tab:gradient}
\end{table}

\begin{table}[htp]
    \centering
    \begin{tabular}{ |c|c|c|c|c| } 
        \hline
        \multicolumn{5}{ |c| }{\textbf{Conjugate gradient method}} \\
        \hline
        \textbf{Matrix} & \textbf{Tolerance} & \textbf{Relative error} & \textbf{Iterations} & \textbf{Time (s)} \\
        \hline
        \multirow{4}{*}{\textbf{spa1}} & $10^{-4}$ & 0.0208 & 49 & 0.8945 \\
        \cline{2-5}
        & $10^{-6}$ & \num{2.5529e-05} & 134 & 2.6087 \\
        \cline{2-5}
        & $10^{-8}$ & \num{1.3198e-07} & 177 & 3.2474 \\
        \cline{2-5}
        & $10^{-10}$ & \num{1.2123e-09} & 200 & 3.8060 \\
        \hline
        \multirow{4}{*}{\textbf{spa2}} & $10^{-4}$ & 0.00982 & 42 & 6.8187 \\
        \cline{2-5}
        & $10^{-6}$ & 0.0001 & 122 & 21.8009 \\
        \cline{2-5}
        & $10^{-8}$ & \num{5.5867e-07} & 196 & 33.5633 \\
        \cline{2-5}
        & $10^{-10}$ & \num{5.3242e-09} & 240 & 35.9954 \\
        \hline
        \multirow{4}{*}{\textbf{vem1}} & $10^{-4}$ & \num{4.0828e-05} & 38 & 0.4312 \\
        \cline{2-5}
        & $10^{-6}$ & \num{3.7323e-07} & 45 & 0.3090 \\
        \cline{2-5}
        & $10^{-8}$ & \num{2.8319e-09} & 53 & 0.3247 \\
        \cline{2-5}
        & $10^{-10}$ & \num{2.1918e-11} & 59 & 0.3690 \\
        \hline
        \multirow{4}{*}{\textbf{vem2}} & $10^{-4}$ & \num{5.7290e-05} & 47 & 0.3039 \\
        \cline{2-5}
        & $10^{-6}$ & \num{4.7430e-07} & 56 & 0.3647 \\
        \cline{2-5}
        & $10^{-8}$ & \num{4.3000e-09} & 66 & 0.4018 \\
        \cline{2-5}
        & $10^{-10}$ & \num{2.2476e-11} & 74 & 0.4815 \\
        \hline
    \end{tabular}
    \caption{Summary of the results obtained using conjugate gradient method.}
    \label{tab:conjugate-gradient}
\end{table}

\section{Considerations}

These results show how the matrices dimensions influence the resolution execution time, while the number of iterations needed to converge, adjusted with tolerance, is influenced by matrix coefficients.

Jacobi and Gauß-Seidel methods seems to work better on \textit{spa1} and \textit{spa2} matrices, especially regarding times.

The conjugate gradient method, instead, it's by far the best method when dealing with \textit{vem1} and \textit{vem2} matrices.

The gradient method is really slow when solving \textit{spa1} and \textit{spa2} but it gets better with the other two.

In conclusion, the results indicate that the conjugate gradient method is the most efficient method, globally.

\printbibliography

\end{document}
