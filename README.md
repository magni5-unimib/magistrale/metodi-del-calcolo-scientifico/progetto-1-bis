# Progetto 1-bis

Implementation of a Python library to execute the following linear systems solver methods: Jacobi, Gauß-Seidel, gradient, conjugate gradient.
    
The use of the library is limited to symmetric, positive-definite matrices.
    
Results of the execution of the methods are shown and discussed..

## How-to

Bash commands to set up python virtual environment and install needed libraries.

The last command executes the main script.

```bash
cd LinearSystemsSolver
pyhton -m venv env
source env/bin/activate
pip install -r requirements.txt
pyhton main.py
```

## Report

Check out the [Report](Report/Report.pdf) for more.