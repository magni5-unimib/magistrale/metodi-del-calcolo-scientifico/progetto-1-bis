from scipy import sparse
from scipy.io import mmread
from utils import io, matrix_utilities, settings


print("\nLinearSystemsSolver")

file_name = io.choose_file(settings.DATA_PATH)
tolerance = io.choose_in_list("tolerances", settings.TOLERANCES)

a = mmread(settings.DATA_PATH + "/" + file_name)

x_solution = sparse.coo_array([1.0] * len(a.A))
x_approx = sparse.coo_array([0.0] * len(a.A))

b = a @ x_solution.transpose()

for m in settings.METHODS :
    method = m()
    print("\n" + method.name())
    my_solution = method.solve(a, b, x_approx, tolerance, int(settings.MAX_ITER))
    print("Time:", method.time())
    print("Relative error:", method.relative_error(my_solution, x_solution))