from .method import Method
from utils import matrix_utilities


class Jacobi(Method):

    residual = None

    def name(self) :
        return "Jacobi"

    def before_solve(self) :
        return

    def found_solution(self) :
        return matrix_utilities.check_sparse_solution_residual(
            self.residual, 
            self.b, 
            self.tolerance
        )

    def relative_error(self, my_solution, solution) :
        return matrix_utilities.calculate_relative_error(my_solution, solution)

    def step(self) :
        d = matrix_utilities.get_inverted_diagonal_matrix(self.a)
        self.x = self.x.transpose()
        self.residual = self.b - (self.a @ self.x) 
        self.x = self.x + (d @ self.residual)
        self.x = self.x.transpose()