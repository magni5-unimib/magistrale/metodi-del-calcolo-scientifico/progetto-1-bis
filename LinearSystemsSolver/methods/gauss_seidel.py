from .method import Method
from scipy import sparse
from utils import matrix_utilities


class GaussSeidel(Method):

    residual = None

    def name(self) :
        return "Gauß-Seidel"

    def before_solve(self) :
        return

    def found_solution(self) :
        return matrix_utilities.check_sparse_solution_residual(
            self.residual, 
            self.b, 
            self.tolerance
        )

    def relative_error(self, my_solution, solution) :
        return matrix_utilities.calculate_relative_error(my_solution, solution)

    def step(self) :
        self.residual = self.b - (self.a @ self.x.transpose())
        l = sparse.tril(self.a, 0)
        y = sparse.linalg.spsolve_triangular(l.tocsr(), self.residual.toarray())
        y = sparse.coo_array(y)
        self.x = self.x + y.transpose()