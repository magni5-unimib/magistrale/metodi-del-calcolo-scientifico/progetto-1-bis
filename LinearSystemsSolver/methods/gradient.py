from .method import Method
from utils import matrix_utilities


class Gradient(Method):

    def name(self) :
        return "Gradient"

    def before_solve(self) :
        self.x = self.x.transpose()

    def found_solution(self) :
        return matrix_utilities.check_sparse_solution(
            self.a, 
            self.x.transpose(), 
            self.b, 
            self.tolerance
        )

    def relative_error(self, my_solution, solution) :
        return matrix_utilities.calculate_relative_error(my_solution.transpose(), solution)

    def step(self) :
        r = self.b - self.a @ self.x
        y = self.a @ r
        num_alpha = r.transpose().dot(r)
        den_alpha = r.transpose().dot(y)
        alpha = num_alpha[0,0] / den_alpha[0,0]
        self.x = self.x + alpha * r