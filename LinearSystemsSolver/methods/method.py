from time import time
from utils import matrix_utilities


class Method:

    current_iteration = 0
    max_iter = None

    a = None
    b = None
    x = None
    tolerance = None

    start = None
    end = None

    def reached_max_iter(self) :
        return self.max_iter != None and self.current_iteration >= self.max_iter

    def time(self) :
        if self.start != None and self.end != None:
            return self.end - self.start

    def solve(self, a, b, x, tolerance, max_iter) :
        if not check_matrix(a):
            return None

        self.a = a
        self.b = b
        self.x = x
        self.tolerance = tolerance
        self.max_iter = max_iter

        self.start = time()

        self.before_solve()

        while self.current_iteration == 0 or not(self.found_solution() or self.reached_max_iter()):
            self.current_iteration += 1
            self.step()

        self.end = time()

        if self.reached_max_iter():
            print("Reached maximum number of iterations", "(" + self.max_iter + ")", "and no solution has been found.")
            return None
        else:
            print("Solution has been found in", self.current_iteration, "iterations.")
            return self.x

    def name(self) :
        raise NotImplementedError("Subclasses must override name(self)")

    def before_solve(self) :
        raise NotImplementedError("Subclasses must override before_solve(self)")

    def found_solution(self) :
        raise NotImplementedError("Subclasses must override found_solution(self)")

    def relative_error(self, my_solution, solution) :
        raise NotImplementedError("Subclasses must override relative_error(self, my_solution, solution)")

    def step(self) :
        raise NotImplementedError("Subclasses must override step(self)")


def check_matrix(a) :
    if not matrix_utilities.is_cholesky_decomposable(a.toarray()):
        print("Matrix is not positive definite")
        return False
    # other checks if needed
    return True