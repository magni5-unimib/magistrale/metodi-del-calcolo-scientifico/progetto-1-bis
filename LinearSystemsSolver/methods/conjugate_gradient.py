from .method import Method
from utils import matrix_utilities


class ConjugateGradient(Method):

    d = None

    def name(self) :
        return "Conjugate gradient"

    def before_solve(self) :
        self.x = self.x.transpose()
        if self.d == None:
            # se d non è specificata allora d = r
            self.d = self.b - self.a @ self.x

    def found_solution(self) :
        return matrix_utilities.check_sparse_solution(
            self.a, 
            self.x.transpose(), 
            self.b, 
            self.tolerance
        )

    def relative_error(self, my_solution, solution) :
        return matrix_utilities.calculate_relative_error(my_solution.transpose(), solution)

    def step(self) :
        r = self.b - self.a @ self.x
        y = self.a @ self.d
        z = self.a @ r
        num_alpha = self.d.transpose().dot(r)
        den_alpha = self.d.transpose().dot(y)
        alpha = num_alpha[0,0] / den_alpha[0,0]
        self.x = self.x + alpha * self.d
        r = self.b - self.a @ self.x
        w = self.a @ r
        num_beta = self.d.transpose().dot(w)
        den_beta = self.d.transpose().dot(y)
        beta = num_beta[0,0] / den_beta[0,0]
        self.d = r - beta * self.d