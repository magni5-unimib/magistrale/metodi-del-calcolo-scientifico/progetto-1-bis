from methods.jacobi import Jacobi
from methods.gauss_seidel import GaussSeidel
from methods.gradient import Gradient
from methods.conjugate_gradient import ConjugateGradient


DATA_PATH = "data"

TOLERANCES = [
    10**-4, 
    10**-6, 
    10**-8, 
    10**-10
]

METHODS = [
    Jacobi,
    GaussSeidel,
    Gradient,
    ConjugateGradient
]

MAX_ITER = 20000