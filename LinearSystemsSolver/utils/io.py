import os
import sys


def choose_file(path) :
    files = [
        f for f in sorted(os.listdir(path))
        if os.path.isfile(os.path.join(path, f)) and not f.startswith('.')
    ]
    return choose_in_list("files", files)

def choose_in_list(name, list) :
    print("\n" + name.capitalize(), "list")

    i = 1
    for item in list :
        print(i, "-", item)
        i += 1

    print("0 - exit")

    try:
        choice = int(input("\nChoose by index: "))    
    except:
        print("Input not a number. Try again.")
        return choose_in_list(name, list)
    
    if choice == 0:
        print("Thank you.\nQuitting...\n")
        sys.exit(0)
    elif 1 <= choice <= len(list):
        return list[choice - 1]
    else:
        print("Input not valid. Try again.")
        return choose_in_list(name, list)