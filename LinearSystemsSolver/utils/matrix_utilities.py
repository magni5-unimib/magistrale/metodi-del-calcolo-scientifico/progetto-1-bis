import numpy
from scipy import sparse


def is_cholesky_decomposable(a) :
    try:
        ch = numpy.linalg.cholesky(a)
        return True
    except numpy.linalg.LinAlgError as e:
        #if e.args[0] == 'Matrix is not positive definite' :
        return False
        #else :
            #raise er
            
def check_sparse_solution(A, x, b, tol) : 
    residual = A.dot(x.transpose()) - b
    value = sparse.linalg.norm(residual) / (sparse.linalg.norm(b))
    if(value < tol) :
         return True
    else :
         return False

def check_sparse_solution_residual(residual, b, tol) :
    value = sparse.linalg.norm(residual) / (sparse.linalg.norm(b))
    if(value < tol) :
         return True
    else :
         return False

def calculate_relative_error(x_approx, x_solution) :
    return sparse.linalg.norm(x_approx - x_solution) / sparse.linalg.norm(x_solution)

def get_inverted_diagonal_matrix(a) :
    diag = a.diagonal()
    for i in range(len(diag)) :
        diag[i] = 1/diag[i]
    return sparse.diags(diag, 0)
